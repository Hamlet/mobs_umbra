--[[
	Mobs Umbra - Adds a shadow-looking Non Playing Character.
	Copyright © 2019, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Procedures
--

-- Check for free space and place a new node
mobs_umbra.pr_PlaceNode = function(a_v_position)

	local s_oldNodeName = minetest.get_node(a_v_position).name

	if (s_oldNodeName == 'air') then
		minetest.set_node(a_v_position, {name = 'mobs_umbra:obscure_node'})
	end
end


-- Used to create a cube of nodes around the target
mobs_umbra.pr_DarkCube = function(a_v_position, a_i_offset)
	local v_coordinates = {y = 0.0, x = 0.0, z = 0.0}

	for i_value = -a_i_offset, a_i_offset do
		v_coordinates.x = (a_v_position.x + i_value)

		for i_value = -a_i_offset, a_i_offset do
			v_coordinates.z = (a_v_position.z + i_value)

			for i_value = -a_i_offset, a_i_offset do
				v_coordinates.y = (a_v_position.y + i_value)
				mobs_umbra.pr_PlaceNode(v_coordinates)
			end
		end
	end
end
