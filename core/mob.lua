--[[
	Mobs Umbra - Adds a shadow-looking Non Playing Character.
	Copyright © 2019, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Mobile entity
--

mobs:register_mob('mobs_umbra:umbra', {
	--nametag = 'Umbra',
	type = 'npc',
	hp_min = minetest.PLAYER_MAX_HP_DEFAULT,
	hp_max = minetest.PLAYER_MAX_HP_DEFAULT,
	armor = 100,				-- Same as player
	walk_velocity = 4,			-- Same as player
	run_velocity = 5,
	stand_chance = 50,			-- 50%
	walk_chance = 50,			-- 50%
	jump = true,				-- Same as player
	jump_height = 6,
	pushable = false,			-- Same as player
	view_range = 16,			-- A map-chunk
	damage = 1,					-- Same as player (bare hands)
	knock_back = false,			-- Same as player
	fear_height = 0,			-- Disabled
	fall_damage = false,
	water_damage = 0,			-- Disabled
	lava_damage = 20,			-- Lava = super light source.
	light_damage_min = (minetest.LIGHT_MAX / 2),
	light_damage_max = minetest.LIGHT_MAX,
	suffocation = 0,			-- Disabled
	floats = 0,					-- Disabled
	reach = 4,					-- Same as player
	attack_chance = 25,			-- 25%
	attack_monsters = true,
	attack_npcs = true,
	attack_players = true,
	group_attack = true,
	attack_type = 'dogshoot',
	arrow = 'mobs_umbra:sagitta_obscura',
	pathfinding = 1,			-- Enabled when in melee range
	--[[
	dogshoot_switch = 1,
	dogshoot_count_max = 3,
	dogshoot_count2_max = 6,
	--]]
	shoot_interval = 1.5,
	shoot_offset = 0.5,
	makes_footstep_sound = false,	-- Disabled
	drops = {
		{name = 'default:gold_lump', chance = 75, min = 1, max = 5},
		{name = 'mobs_umbra:dark_arrow', chance = 8, min = 1, max = 1}
	},
	visual = 'mesh',
	visual_size = {x = 1.0, y = 1.0, z = 0.01},
	collisionbox = {-0.4, 0.0, -0.4, 0.4, 1.7, 0.4},
	selectionbox = {-0.4, 0.0, -0.4, 0.4, 1.7, 0.4},
	textures = {'character.png^[colorize:black:255'},
	mesh = 'character.b3d',
	animation = {
		stand_start = 0,
		stand_end = 79,
		stand_speed = 30,

		walk_start = 168,
		walk_end = 187,
		walk_speed = 30,

		run_start = 168,
		run_end = 187,
		run_speed = 30,

		punch_start = 189,
		punch_end = 198,
		punch_speed = 30,

		die_start = 162,
		die_end = 166,
		die_speed = 0.8
	}
})
