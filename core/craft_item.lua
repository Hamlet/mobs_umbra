--[[
	Mobs Umbra - Adds a shadow-looking Non Playing Character.
	Copyright © 2019, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Umbra's dark arrow
--

-- Rarely dropped when the mob dies
minetest.register_craftitem('mobs_umbra:dark_arrow', {
	description = mobs_umbra.l10n('Dark Arrow'),
	inventory_image = 'mobs_umbra_dark_arrow.png',
	wield_scale = {x = 0.5, y = 0.5, z = 0.5}
})

minetest.register_alias('dark_arrow', 'mobs_umbra:dark_arrow')
