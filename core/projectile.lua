--[[
	Mobs Umbra - Adds a shadow-looking Non Playing Character.
	Copyright © 2019, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Mobile arrow
--

mobs:register_arrow('mobs_umbra:sagitta_obscura', {

	visual = 'sprite',
	visual_size = {x = 0.5, y = 0.5},
	textures = {'mobs_umbra_projectile.png'},
	velocity = 35,		-- Nodes per second


	hit_player = function(self, player)

		--[[
			Minetest v5.2.0 lua_api.txt at line 1853

			object:punch(puncher, time_from_last_punch, tool_capabilities,
				direction)
		--]]

		local v_position = self.object:get_pos()

		player:punch(self.object, 1.0, {
				full_punch_interval = 1.0,
				damage_groups = {fleshy = 2}
			},
			nil
		)

		mobs_umbra.pr_DarkCube(v_position, mobs_umbra.darkCubeOffset)
	end,


	hit_mob = function(self, player)

		local v_position = self.object:get_pos()

		-- See "hit player" for the explanation.
		player:punch(self.object, 1.0, {
				full_punch_interval = 1.0,
				damage_groups = {fleshy = 2}
			},
			nil
		)

		mobs_umbra.pr_DarkCube(v_position, mobs_umbra.darkCubeOffset)
	end,

	--[[
	hit_object = function(self, player)

		local v_position = self.object:get_pos()

		-- See "hit player" for the explanation.
		player:punch(self.object, 1.0, {
				full_punch_interval = 1.0,
				damage_groups = {fleshy = 2}
			},
			nil
		)

		mobs_umbra.pr_DarkCube(v_position, mobs_umbra.darkCubeOffset)
	end,
	--]]


	hit_node = function(self, pos, node)

		local v_position = self.object:get_pos()

		mobs_umbra.pr_DarkCube(v_position, mobs_umbra.darkCubeOffset)
	end
})
