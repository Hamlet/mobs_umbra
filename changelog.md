# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/).


## [Unreleased]



## [0.2.1] - 2020-07-29
### Removed

	- Projectile's "hit object" field, causing crashes.


## [0.2.0] - 2020-07-07
### Added

	- Unique item drop.



## [0.1.2] - 2020-07-02
### Changed

	- Code rewritten from scratch and split into different files.

### Added

	- Options to tweak mob's spawning options, see Settings -> All Settings -> mobs_umbra

### Removed

	- Support for Minetest Game v0.4.x



## [0.1.1] - 2020-05-02
### Changed

	- Fixed suffocation setting which caused a bug.



## [0.1.0] - 2019-11-08
### Added

	- Initial release.
